
package provesserviceclient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the provesserviceclient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DemanarProva_QNAME = new QName("http://ProvesService/", "demanarProva");
    private final static QName _DemanarProvaResponse_QNAME = new QName("http://ProvesService/", "demanarProvaResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: provesserviceclient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DemanarProvaResponse }
     * 
     */
    public DemanarProvaResponse createDemanarProvaResponse() {
        return new DemanarProvaResponse();
    }

    /**
     * Create an instance of {@link DemanarProva }
     * 
     */
    public DemanarProva createDemanarProva() {
        return new DemanarProva();
    }

    /**
     * Create an instance of {@link ReservaType }
     * 
     */
    public ReservaType createReservaType() {
        return new ReservaType();
    }

    /**
     * Create an instance of {@link Pacient }
     * 
     */
    public Pacient createPacient() {
        return new Pacient();
    }

    /**
     * Create an instance of {@link ProvaType }
     * 
     */
    public ProvaType createProvaType() {
        return new ProvaType();
    }

    /**
     * Create an instance of {@link PacientType }
     * 
     */
    public PacientType createPacientType() {
        return new PacientType();
    }

    /**
     * Create an instance of {@link ServeiMedic }
     * 
     */
    public ServeiMedic createServeiMedic() {
        return new ServeiMedic();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DemanarProva }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ProvesService/", name = "demanarProva")
    public JAXBElement<DemanarProva> createDemanarProva(DemanarProva value) {
        return new JAXBElement<DemanarProva>(_DemanarProva_QNAME, DemanarProva.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DemanarProvaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ProvesService/", name = "demanarProvaResponse")
    public JAXBElement<DemanarProvaResponse> createDemanarProvaResponse(DemanarProvaResponse value) {
        return new JAXBElement<DemanarProvaResponse>(_DemanarProvaResponse_QNAME, DemanarProvaResponse.class, null, value);
    }

}
