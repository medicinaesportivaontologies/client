
package provesserviceclient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ReservaType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReservaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="reservaID" type="{http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves}ReservaDATA" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enElSistema" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nomProveidor" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="finsAlDia" type="{http://www.w3.org/2001/XMLSchema}dateTime" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="desDelDia" type="{http://www.w3.org/2001/XMLSchema}dateTime" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="delSolicitant" type="{http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves}Pacient" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="pelServei" type="{http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves}ServeiMedic" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReservaType", propOrder = {
    "reservaID",
    "name",
    "enElSistema",
    "nomProveidor",
    "finsAlDia",
    "desDelDia",
    "delSolicitant",
    "pelServei"
})
public class ReservaType {

    protected ReservaDATA reservaID;
    protected String name;
    @XmlElement(nillable = true)
    protected List<String> enElSistema;
    @XmlElement(nillable = true)
    protected List<String> nomProveidor;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected List<XMLGregorianCalendar> finsAlDia;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected List<XMLGregorianCalendar> desDelDia;
    @XmlElement(nillable = true)
    protected List<Pacient> delSolicitant;
    @XmlElement(nillable = true)
    protected List<ServeiMedic> pelServei;

    /**
     * Gets the value of the reservaID property.
     * 
     * @return
     *     possible object is
     *     {@link ReservaDATA }
     *     
     */
    public ReservaDATA getReservaID() {
        return reservaID;
    }

    /**
     * Sets the value of the reservaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReservaDATA }
     *     
     */
    public void setReservaID(ReservaDATA value) {
        this.reservaID = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the enElSistema property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the enElSistema property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEnElSistema().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getEnElSistema() {
        if (enElSistema == null) {
            enElSistema = new ArrayList<String>();
        }
        return this.enElSistema;
    }

    /**
     * Gets the value of the nomProveidor property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nomProveidor property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNomProveidor().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getNomProveidor() {
        if (nomProveidor == null) {
            nomProveidor = new ArrayList<String>();
        }
        return this.nomProveidor;
    }

    /**
     * Gets the value of the finsAlDia property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the finsAlDia property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFinsAlDia().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XMLGregorianCalendar }
     * 
     * 
     */
    public List<XMLGregorianCalendar> getFinsAlDia() {
        if (finsAlDia == null) {
            finsAlDia = new ArrayList<XMLGregorianCalendar>();
        }
        return this.finsAlDia;
    }

    /**
     * Gets the value of the desDelDia property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the desDelDia property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDesDelDia().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XMLGregorianCalendar }
     * 
     * 
     */
    public List<XMLGregorianCalendar> getDesDelDia() {
        if (desDelDia == null) {
            desDelDia = new ArrayList<XMLGregorianCalendar>();
        }
        return this.desDelDia;
    }

    /**
     * Gets the value of the delSolicitant property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the delSolicitant property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDelSolicitant().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Pacient }
     * 
     * 
     */
    public List<Pacient> getDelSolicitant() {
        if (delSolicitant == null) {
            delSolicitant = new ArrayList<Pacient>();
        }
        return this.delSolicitant;
    }

    /**
     * Gets the value of the pelServei property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pelServei property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPelServei().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServeiMedic }
     * 
     * 
     */
    public List<ServeiMedic> getPelServei() {
        if (pelServei == null) {
            pelServei = new ArrayList<ServeiMedic>();
        }
        return this.pelServei;
    }

}
