
package provesserviceclient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProvaDATA.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ProvaDATA">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Prova_1"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ProvaDATA")
@XmlEnum
public enum ProvaDATA {

    @XmlEnumValue("Prova_1")
    PROVA_1("Prova_1");
    private final String value;

    ProvaDATA(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ProvaDATA fromValue(String v) {
        for (ProvaDATA c: ProvaDATA.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
