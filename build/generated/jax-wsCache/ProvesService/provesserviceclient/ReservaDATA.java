
package provesserviceclient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReservaDATA.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReservaDATA">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Reserva_4"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReservaDATA")
@XmlEnum
public enum ReservaDATA {

    @XmlEnumValue("Reserva_4")
    RESERVA_4("Reserva_4");
    private final String value;

    ReservaDATA(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReservaDATA fromValue(String v) {
        for (ReservaDATA c: ReservaDATA.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
