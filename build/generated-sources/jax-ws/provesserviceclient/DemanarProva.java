
package provesserviceclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for demanarProva complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="demanarProva">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves}ProvaType" minOccurs="0"/>
 *         &lt;element name="arg1" type="{http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves}PacientType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "demanarProva", namespace = "http://ProvesService/", propOrder = {
    "arg0",
    "arg1"
})
public class DemanarProva {

    protected ProvaType arg0;
    protected PacientType arg1;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link ProvaType }
     *     
     */
    public ProvaType getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProvaType }
     *     
     */
    public void setArg0(ProvaType value) {
        this.arg0 = value;
    }

    /**
     * Gets the value of the arg1 property.
     * 
     * @return
     *     possible object is
     *     {@link PacientType }
     *     
     */
    public PacientType getArg1() {
        return arg1;
    }

    /**
     * Sets the value of the arg1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PacientType }
     *     
     */
    public void setArg1(PacientType value) {
        this.arg1 = value;
    }

}
