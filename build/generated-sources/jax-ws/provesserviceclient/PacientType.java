
package provesserviceclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PacientType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PacientType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pacientID" type="{http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves}PacientDATA" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cognom2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cognom1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PacientType", propOrder = {
    "pacientID",
    "name",
    "cognom2",
    "cognom1",
    "nom",
    "dni"
})
public class PacientType {

    protected PacientDATA pacientID;
    protected String name;
    protected String cognom2;
    protected String cognom1;
    protected String nom;
    protected String dni;

    /**
     * Gets the value of the pacientID property.
     * 
     * @return
     *     possible object is
     *     {@link PacientDATA }
     *     
     */
    public PacientDATA getPacientID() {
        return pacientID;
    }

    /**
     * Sets the value of the pacientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link PacientDATA }
     *     
     */
    public void setPacientID(PacientDATA value) {
        this.pacientID = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the cognom2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCognom2() {
        return cognom2;
    }

    /**
     * Sets the value of the cognom2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCognom2(String value) {
        this.cognom2 = value;
    }

    /**
     * Gets the value of the cognom1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCognom1() {
        return cognom1;
    }

    /**
     * Sets the value of the cognom1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCognom1(String value) {
        this.cognom1 = value;
    }

    /**
     * Gets the value of the nom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets the value of the nom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNom(String value) {
        this.nom = value;
    }

    /**
     * Gets the value of the dni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDni() {
        return dni;
    }

    /**
     * Sets the value of the dni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDni(String value) {
        this.dni = value;
    }

}
