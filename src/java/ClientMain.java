

import com.github.tranchis.caller.Caller;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import matchmakerclient.MatchMaker;
import matchmakerclient.MatchMakerWSImpl;
import matchmakerclient.MatchResult;
import provesserviceclient.PacientDATA;
import provesserviceclient.PacientType;
import provesserviceclient.ProvaType;
import provesserviceclient.ReservaType;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Neville
 */
public class ClientMain {
    
    public static void main (String args[])
    {
        MatchMaker mm = new MatchMaker();
        MatchMakerWSImpl mmp = mm.getMatchMakerPort();
        
        List<String> inputs = new LinkedList<String>();
        inputs.add("http://medicinaesportivaontologies.bitbucket.org/pacient.owl#Pacient");
        inputs.add("http://medicinaesportivaontologies.bitbucket.org/serveimedic.owl#Prova");
        List<String> outputs = new LinkedList<String>();
        outputs.add("http://medicinaesportivaontologies.bitbucket.org/documents.owl#Reserva");
    
        List<MatchResult> lmr = new LinkedList<MatchResult>();
        lmr.addAll(mmp.performSignatureMatchWithParams(inputs, outputs,true,false) );
        
        Random continuar = new Random();
        boolean b = false;
        while (!b)
        {
            b = continuar.nextBoolean();
            Caller c = new Caller();
            PacientType pacient = new PacientType();
            pacient.setDni("111");
            pacient.setNom("antonio");
            pacient.setCognom1("suarez");
            pacient.setCognom2("galan");
            ProvaType pt = new ProvaType();
            pt.setNom("radiografia");
            Object[] inputclasses = {pt, pacient};
            Object result = c.callService(lmr.get(0).getService(), inputclasses, ReservaType.class);
        }
    }
    
}
